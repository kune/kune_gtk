use gtk::prelude::*;

type NativeWidget = gtk::Widget;

pub struct GtkBackend {
    _p: std::marker::PhantomData<GtkBackend>
}

impl GtkBackend {
    pub fn new() -> GtkBackend {
        gtk::init().unwrap();

        GtkBackend {
            _p: std::marker::PhantomData
        }
    }
}

impl kune::Backend for GtkBackend {
    type NativeWidget = NativeWidget;

    fn main_loop(&self) {
        gtk::main();
    }
}

#[derive(Clone)]
pub struct Button(gtk::Button);

impl kune::widgets::Widget<GtkBackend> for Button {
    fn get_native_widget(&self) -> &NativeWidget {
        &self.0.upcast_ref()
    }
}

impl kune::widgets::Button<GtkBackend> for Button {
    fn set_label(&self, label: &str) {
        self.0.set_label(label);
    }
    fn add_click_listener(&self, handler: impl Fn() + 'static) {
        self.0.connect_clicked(move |_| handler());
    }
}

impl kune::provides::ProvidesButton for GtkBackend {
    type Button = Button;

    fn create_button(&self) -> Button {
        let btn = gtk::Button::new();
        btn.show();
        Button(btn)
    }
}

impl kune::provides::ProvidesCore for GtkBackend {}

pub struct Window(gtk::Window);

impl kune::Window<GtkBackend> for Window {
    fn set_title(&self, title: &str) {
        self.0.set_title(title);
    }

    fn set_native_widget(&self, widget: &NativeWidget) {
        self.0.add(widget);
    }

    fn show(&self) {
        self.0.show();
    }
}

impl kune::provides::ProvidesWindow for GtkBackend {
    type Window = Window;

    fn create_window(&self) -> Window {
        Window(gtk::Window::new(gtk::WindowType::Toplevel))
    }
}
