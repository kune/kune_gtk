use kune::Backend;
use kune::provides::*;
use kune::Window;
use kune::widgets::Button;

fn main() {
    let backend = kune_gtk::GtkBackend::new();

    let window = backend.create_window();
    let button = backend.create_button();

    button.set_label("Button");
    window.set_title("ButtonWindow");

    window.set_widget(&button);
    window.show();
    backend.main_loop();
}
